autoload -Uz compinit && compinit
autoload -U +X bashcompinit && bashcompinit

MAKERS_SRC=~/.cargo/registry/src/index.crates.io-*/cargo-make-*
[ -d $MAKERS_SRC ] && source $MAKERS_SRC/extra/shell/makers-completion.bash

eval "$(starship init zsh)"
alias ll='ls -alF --color=auto'

WORDCHARS='*?_-.[]~=&;!#$%^(){}<>'

export EDITOR=emacs

unsetopt share_history
