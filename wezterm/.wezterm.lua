local wezterm = require 'wezterm'

local config = wezterm.config_builder()

local act = wezterm.action

config.color_scheme = 'AdventureTime'

config.adjust_window_size_when_changing_font_size = false
config.font_size = 20

config.window_padding = { left = 0, right = 0, top = 0, bottom = 0 }

config.keys = {
  { key = 'LeftArrow', mods = 'OPT', action = act.SendKey { key = 'b', mods = 'ALT' } },
  { key = 'RightArrow', mods = 'OPT', action = act.SendKey { key = 'f', mods = 'ALT' } },
  { key = 'LeftArrow', mods = 'CTRL', action = act.SendKey { key = 'b', mods = 'ALT' } },
  { key = 'RightArrow', mods = 'CTRL', action = act.SendKey { key = 'f', mods = 'ALT' } },
  { key = '-', mods = 'CTRL|SHIFT', action = act.DisableDefaultAssignment },
  { key = '=', mods = 'CTRL', action = act.DisableDefaultAssignment },
  { key = '-', mods = 'CTRL', action = act.DisableDefaultAssignment },
  { key = '_', mods = 'CTRL|SHIFT', action = act.DisableDefaultAssignment },
  { key = 'k', mods = 'SUPER', action = act.ResetTerminal },
}
    
return config
