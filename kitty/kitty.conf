# vim:fileencoding=utf-8:ft=conf:foldmethod=marker

# https://sw.kovidgoyal.net/kitty - The fast, feature-rich, GPU based terminal emulator

# This config is optimized for MacOS and uses https://rubjo.github.io/victor-mono font
term xterm-256color

macos_option_as_alt yes
map ctrl+shift+- noop
map cmd+c       copy_to_clipboard
map cmd+v       paste_from_clipboard
map ctrl+left   send_text all \x1b\x62
map ctrl+right  send_text all \x1b\x66
map alt+left    send_text all \x1b\x62
map alt+right   send_text all \x1b\x66

map cmd+1 goto_tab 1
map cmd+2 goto_tab 2
map cmd+3 goto_tab 3
map cmd+4 goto_tab 4
map cmd+5 goto_tab 5
map cmd+6 goto_tab 6
map cmd+7 goto_tab 7
map cmd+8 goto_tab 8
map cmd+9 goto_tab 9

map cmd+k clear_terminal scrollback active

# tab_bar_edge top
url_style single
open_url_modifiers ctrl

font_family		Victor Mono Bold
bold_font		Victor Mono Bold
italic_font		Victor Mono Italic
bold_italic_font	Victor Mono Bold Italic

font_size 16.0

# other color-schemes at https://github.com/dexpota/kitty-themes
# dark background
background            #000000
foreground            #f1f1f1
cursor                #7f7f7f
selection_background  #b4d5ff
color0                #4f4f4f
color8                #7b7b7b
color1                #fa6c5f
color9                #fcb6af
color2                #a8fe60
color10               #ceffab
color3                #fffeb6
color11               #fffecc
color4                #96cafd
color12               #b5dcfe
color5                #fa72fc
color13               #fb9bfe
color6                #c6c4fd
color14               #dfdffd
color7                #eeedee
color15               #fefffe
selection_foreground #000000

# light background
#background            #f4f4f4
#foreground            #3e3e3e
#cursor                #3f3f3f
#selection_background  #a9c1e2
#color0                #3e3e3e
#color8                #666666
#color1                #970b16
#color9                #de0000
#color2                #07962a
#color10               #87d5a2
#color3                #cec501
#color11               #f0cf06
#color4                #003e8a
#color12               #2e6cba
#color5                #e94691
#color13               #ffa29f
#color6                #89d1ec
#color14               #1cfafe
#color7                #ffffff
#color15               #ffffff
#selection_foreground  #f4f4f4

