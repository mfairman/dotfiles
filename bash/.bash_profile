export EDITOR=emacs
PATH="$HOME/bin:$PATH"

alias ll='ls -alFG'

case "$(uname -s)" in
    Linux*)
        PS1="$?:\[\e]0;\u@\h: \w\a\]${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\][\j] \$ ";;
    Darwin*)
        PS1='$?:\[\e]0;\u@\h: \w\a\]\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\][\j] $ ';;
esac


[ -x /opt/homebrew/bin/brew ] && {
	eval $(/opt/homebrew/bin/brew shellenv)
	PATH="/opt/homebrew/opt/make/libexec/gnubin:$PATH"
}

[ -d $HOME/.cargo ] && . "$HOME/.cargo/env"
